const crypto = require("crypto");

exports.deterministicPartitionKey = (event) => {
  const TRIVIAL_PARTITION_KEY = "0";
  const MAX_PARTITION_KEY_LENGTH = 256;
  let candidate;

  candidate = checkEvent(event);
  candidate = checkCandidate(candidate, TRIVIAL_PARTITION_KEY);
  candidate = checkPartitionsKeyLength(candidate, MAX_PARTITION_KEY_LENGTH);

  return candidate;
};

checkEvent = (event) =>{
  if (event) {
    if (event.partitionKey)  return event.partitionKey;
      const data = stringifyData(event)
      return makeUpdate(data);
  }
  
}

stringifyData = (data) => {
  return  JSON.stringify(data);
}

checkCandidate = (candidate, TRIVIAL_PARTITION_KEY) =>{
  if (candidate) {
    if (typeof candidate !== "string") return stringifyData(candidate);
  } else {
    candidate = TRIVIAL_PARTITION_KEY;
    return candidate;
  }
}

checkPartitionsKeyLength = (candidate, MAX_PARTITION_KEY_LENGTH) =>{
  if (candidate.length > MAX_PARTITION_KEY_LENGTH) return makeUpdate(candidate);
  return candidate;
}

makeUpdate = (record) =>{
  return crypto.createHash("sha3-512").update(record).digest("hex");
}


/**
 * Determine independent units of code that can be used to achieve a solution and use it as a guide on to break the code down
 * Ensure reusable code is broken down too as long as it does a more profound task.
 */
